const config = require('./config.json')

module.exports = {
  plugins: {
    'posthtml-expressions': { locals: config },
    'posthtml-include': { root: './src' },
    'posthtml-extend': {},
  },
}
