/* eslint-disable no-console */
const fs = require('fs-extra')
const path = require('path')
const sharp = require('sharp')
const IO = require('fluture')
const R = require('ramda')
const { slideshow } = require('./config.json')

const exists = f => IO.tryP(() => fs.stat(f))
  .fold(() => false, x => x)

const indir = path.resolve(slideshow.folder)
const outdir = path.resolve(slideshow.outfolder)

const resize = ({ width, prefix }) => file => IO.do(function* () {
  const inFile = path.join(indir, file)
  const outFile = path.join(outdir, `${prefix}${file}`)
  if (! (yield exists(inFile))) {
    // TODO: `check line `${line}` in config.json`
    throw new Error(`${file} does not exist in ${indir}`)
  }
  if (yield exists(outFile)) return ['ignored', path.basename(outFile)]
  yield IO.tryP(() => sharp(inFile)
    .resize({ width })
    .toFile(outFile))
  console.log(`${file} -> ${path.basename(outFile)}`)
  return ['resized', file]
})

const resizeSmall = resize({ width: Number(slideshow.smallWidth), prefix: 'S_' })
const resizeXSmall = resize({ width: Number(slideshow.xsmallWidth), prefix: 'XS_' })

IO.do(function* () {
  yield IO.tryP(_ => fs.ensureDir(outdir))
  // for each picture
  const pics = yield IO.parallel(1, slideshow.pictures
    // generate two smaller pictures
    .map(p => IO.parallel(1, [resizeSmall(p), resizeXSmall(p)])))
  const fancy = R.pipe(
    R.reduce(R.concat, []),
    R.groupBy(R.prop(0)),
    R.map(R.length),
  )
  return ['Finished resizing', fancy(pics)]
})
  .fork(console.error, x => console.log(...x))
