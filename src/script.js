import Glide, {
  Images,
  Autoplay,
  Controls,
  Swipe,
} from '@glidejs/glide/dist/glide.modular.esm'

new Glide('.glide', {
  autoplay: 3000,
  hoverpause: true,
  perView: 1,
  type: 'carousel',
})
  .mount({
    Images,
    Autoplay,
    Controls,
    Swipe,
  })
